const supertest = require('supertest');
const app = require('../server');

describe('crear nuevo usuario', ()=> {
  it('responde con 200', (done)=> {
      supertest(app)
      .post('/users/register')
      .send({
        password: "1234",
        user: "5678",
        email: "91112@email.com"
      })
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });
});

describe('iniciar sesion con usuario ',()=>{
  it('responde con 200', (done)=> {
      supertest(app)
      .post('/login')
      .send({
        password: "1234",
        user: "5678",
        email: "91112@email.com"
      })
      .set('Accept', 'application/json')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });
});

describe('obtener lista de usuarios',()=>{
  it('responde con 200', (done)=> {
      supertest(app)
      .get('/users')
      .set('Accept', 'application/json')
      .set('Authorization', 'Bearer eyJhbGciOiJIUzI1NiJ9.NWRkMjRmY2VkMTY3NTEwNzhhYzVmYjNh.5QKIZXhfImko3qCvEHKF6986jk3hzTRrfKSPLdOIa2c')
      .expect(200)
      .end(function(err, res) {
        if (err) return done(err);
        done();
      });
  });
});
