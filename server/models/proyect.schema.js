const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.ObjectId;

const ProyectSchema = mongoose.Schema({
    name: String,
    description : String,
    date_solicited : Date,
    date_start : Date,
    manager : ObjectId,
    owner : ObjectId,
    equip : [ObjectId],
    history_user_cards : [ObjectId],
    closed : Boolean,
    deleted : Boolean
}, {
    timestamps: true
});

ProyectSchema.index({name : 1 });


module.exports = mongoose.model('Proyect', ProyectSchema);
