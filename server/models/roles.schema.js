const mongoose = require('mongoose');

const AbilitySchema = mongoose.Schema({
    name : String,
    description : String
}, {
    timestamps: true
});



AbilitySchema.index({name : 1 });


module.exports = mongoose.model('Ability', AbilitySchema);


