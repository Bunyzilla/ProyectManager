const mongoose = require('mongoose');

const CardListSchema = mongoose.Schema({
    name: String,
    comment : String
}, {
    timestamps: true
});

CardListSchema.index({name : 1 });


module.exports = mongoose.model('CardList', CardListSchema);


