const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.ObjectId;

const CardSchema = mongoose.Schema({
    name: String,
    description : String,
    how : String,
    for : String,
    want : String,
    belongs_to_list : ObjectId,
    priority : String,
    size : Number,
    validated : Boolean,
    state : String,
    date_created : Date,
    belongs_to_user : ObjectId
}, {
    timestamps: true
});


CardSchema.index({name : 1 });


module.exports = mongoose.model('Card', CardSchema);


