const User = require('./../models/user.schema');
//Se importa el logger
const logger = require('./../utils/logger');
const mongoose = require('mongoose');


function list(req, res, next){
    // User.find({}).then(users => {
    //     logger.info(`Enviando lista de usuarios...`);
    //     res.json({
    //         message: res.__('ok'),
    //         error: false,
    //         objs: users
    //     })
    // });
    User.aggregate([
      {$lookup:{
        from: 'roles',
        localField: 'roles',
        foreignField: '_id',
        as: 'roleObjs'
      }},
      {$lookup:{
        from: 'cards',
        localField: 'saved_tarjets',
        foreignField: '_id',
        as: 'tarjetObjs'
      }},
      {$lookup:{
        from: 'cardLists',
        localField: 'saved_lists',
        foreignField: '_id',
        as: 'cardListObjs'
      }},
    ]).then(users=>{
      logger.info(`Enviando lista de usuarios...`);
      res.json({
          message: res.__('ok'),
          error: false,
          objs: users
      });
    })
}

function index(req, res, next){
  let id = req.params.id;
  console.log(`El id de usuario es ${id}`);
  // User.findById( id , (err , user)=>{
  //   logger.info('Enviando el usuario con id...'+id);
  //   res.json({
  //       message: res.__('ok'),
  //       error: false,
  //       objs: user
  //   });
  // });
  User.aggregate([
    {$match:{
      _id: mongoose.Types.ObjectId(`${id}`)
    }},
    {$lookup:{
      from: 'roles',
      localField: 'roles',
      foreignField: '_id',
      as: 'roleObjs'
    }},
    {$lookup:{
      from: 'cards',
      localField: 'saved_tarjets',
      foreignField: '_id',
      as: 'tarjetObjs'
    }},
    {$lookup:{
      from: 'cardLists',
      localField: 'saved_lists',
      foreignField: '_id',
      as: 'cardListObjs'
    }}
  ]).then(users=>{
    logger.info(`Enviando lista de usuarios...`);
    res.json({
        message: res.__('ok'),
        error: false,
        objs: users
    });
  })
}

function listProyects(req, res, next){
  let id = req.params.id;
  User.aggregate([
    {$match:{
      _id: mongoose.Types.ObjectId(`${id}`)
    }},
    {$lookup:{
      from: 'proyects',
      localField: '_id',
      foreignField: 'equip',
      as: 'equipProyects'
    }},
    {$lookup:{
      from: 'proyects',
      localField: '_id',
      foreignField: 'manager',
      as: 'managerProyects'
    }},
    {$lookup:{
      from: 'proyects',
      localField: '_id',
      foreignField: 'owner',
      as: 'ownerProyects'
    }}
  ]).then(users=>{
    logger.info(`Enviando Usuario con proyectos...`);
    res.json({
        message: res.__('ok'),
        error: false,
        objs: users
    });
  })
}

function create(req, res, next){
  let user = req.body;

  let newUser = new User(user);

  newUser.save()
    .then((obj)=>{
        logger.info('Usuario con id '+obj._id+" Creado...");
        User.aggregate([
          {$match:{
            _id: mongoose.Types.ObjectId(`${obj._id}`)
          }},
          {$lookup:{
            from: 'roles',
            localField: 'roles',
            foreignField: '_id',
            as: 'roleObjs'
          }},
          {$lookup:{
            from: 'cards',
            localField: 'saved_tarjets',
            foreignField: '_id',
            as: 'tarjetObjs'
          }},
          {$lookup:{
            from: 'cardLists',
            localField: 'saved_lists',
            foreignField: '_id',
            as: 'cardListObjs'
          }}
        ]).then(users=>{
          res.json({
              message: res.__('ok'),
              error: false,
              objs: users
          });
        })
    })
    .catch((err)=>{
        res.status(500).json({
            message: res.__('error'),
            error: true,
            objs: err
        });
    });
}

function update(req, res, next){
    let newUserData = req.body;
    let id = req.params.id;
    User.findByIdAndUpdate( id , newUserData , (err , user)=>{
        logger.info('Usuario con id '+id+' Actualizado...');
        User.aggregate([
          {$match:{
            _id: mongoose.Types.ObjectId(`${user._id}`)
          }},
          {$lookup:{
            from: 'roles',
            localField: 'roles',
            foreignField: '_id',
            as: 'roleObjs'
          }},
          {$lookup:{
            from: 'cards',
            localField: 'saved_tarjets',
            foreignField: '_id',
            as: 'tarjetObjs'
          }},
          {$lookup:{
            from: 'cardLists',
            localField: 'saved_lists',
            foreignField: '_id',
            as: 'cardListObjs'
          }}
        ]).then(users=>{
          res.json({
              message: res.__('ok'),
              error: false,
              objs: users
          });
        })
      });
}

function destroy(req, res, next){
  let id = req.params.id;
    User.findByIdAndDelete( id , (err , user)=>{
        logger.info('Usuario con id '+id+" Eliminado...");
        User.aggregate([
          {$match:{
            _id: mongoose.Types.ObjectId(`${user._id}`)
          }},
          {$lookup:{
            from: 'roles',
            localField: 'roles',
            foreignField: '_id',
            as: 'roleObjs'
          }},
          {$lookup:{
            from: 'cards',
            localField: 'saved_tarjets',
            foreignField: '_id',
            as: 'tarjetObjs'
          }},
          {$lookup:{
            from: 'cardLists',
            localField: 'saved_lists',
            foreignField: '_id',
            as: 'cardListObjs'
          }}
        ]).then(users=>{
          res.json({
              message: res.__('ok'),
              error: false,
              objs: users
          });
        })
      });
}

module.exports = {
  index, list, listProyects, create, update, destroy
}
