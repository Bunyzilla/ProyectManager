const Proyect = require('../models/proyect.schema');
//Se importa el logger
const logger = require('../utils/logger');
const mongoose = require('mongoose');

function list(req, res, next){
    // Proyect.find({}).then(proyects => {
    //     logger.info(`Enviando todos los proyectos...`);
    //     res.json({
    //         message: res.__('ok'),
    //         error: false,
    //         objs: proyects
    //     })
    // });
    Proyect.aggregate([
      {$lookup:{
        from: 'users',
        localField: 'equip',
        foreignField: '_id',
        as: 'userObjs'
      }},
      {$lookup:{
        from: 'cards',
        localField: 'history_user_cards',
        foreignField: '_id',
        as: 'tarjetObjs'
      }},
      {$lookup:{
        from: 'users',
        localField: 'manager',
        foreignField: '_id',
        as: 'managerObjs'
      }},
      {$lookup:{
        from: 'users',
        localField: 'owner',
        foreignField: '_id',
        as: 'ownerObjs'
      }}
    ]).then(proyects=>{
      res.json({
          message: res.__('ok'),
          error: false,
          objs: proyects
      });
    })
}

function index(req, res, next){
  let id = req.params.id;
  // Proyect.findById( id , (err , proyect)=>{
  //   logger.info('Enviando el proyecto con id...'+id);
  //   res.json({
  //       message: res.__('ok'),
  //       error: false,
  //       objs: proyect
  //   });
  // });
  Proyect.aggregate([
    {$match:{
      _id: mongoose.Types.ObjectId(`${id}`)
    }},
    {$lookup:{
      from: 'users',
      localField: 'equip',
      foreignField: '_id',
      as: 'userObjs'
    }},
    {$lookup:{
      from: 'cards',
      localField: 'history_user_cards',
      foreignField: '_id',
      as: 'tarjetObjs'
    }},
    {$lookup:{
      from: 'users',
      localField: 'manager',
      foreignField: '_id',
      as: 'managerObjs'
    }},
    {$lookup:{
      from: 'users',
      localField: 'owner',
      foreignField: '_id',
      as: 'ownerObjs'
    }}
  ]).then(proyects=>{
    res.json({
        message: res.__('ok'),
        error: false,
        objs: proyects
    });
  })
}

function create(req, res, next){
  let proyect = req.body;

  let newproyect = new Proyect(proyect);

  newproyect.save()
    .then((obj)=>{
        logger.info('Proyecto con id '+obj._id+" Creada...");
        // res.status(200).json({
        //     message: res.__('ok'),
        //     error: false,
        //     objs: obj
        // });
        Proyect.aggregate([
          {$match:{
            _id: mongoose.Types.ObjectId(`${obj._id}`)
          }},
          {$lookup:{
            from: 'users',
            localField: 'equip',
            foreignField: '_id',
            as: 'userObjs'
          }},
          {$lookup:{
            from: 'cards',
            localField: 'history_user_cards',
            foreignField: '_id',
            as: 'tarjetObjs'
          }},
          {$lookup:{
            from: 'users',
            localField: 'manager',
            foreignField: '_id',
            as: 'managerObjs'
          }},
          {$lookup:{
            from: 'users',
            localField: 'owner',
            foreignField: '_id',
            as: 'ownerObjs'
          }}
        ]).then(proyects=>{
          res.json({
              message: res.__('ok'),
              error: false,
              objs: proyects
          });
        })
    })
    .catch((err)=>{
        res.status(200).json({
            message: res.__('error'),
            error: true,
            objs: err
        });
    });
}

function update(req, res, next){
    let newproyectData = req.body;
    let id = req.params.id;
    Proyect.findByIdAndUpdate( id , newproyectData , (err , proyect)=>{
        logger.info('Proyecto con id '+id+' Actualizada...');
        // res.json({
        //     message: res.__('ok'),
        //     error: false,
        //     objs: proyect
        // });
        Proyect.aggregate([
          {$match:{
            _id: mongoose.Types.ObjectId(`${proyect._id}`)
          }},
          {$lookup:{
            from: 'users',
            localField: 'equip',
            foreignField: '_id',
            as: 'userObjs'
          }},
          {$lookup:{
            from: 'cards',
            localField: 'history_user_cards',
            foreignField: '_id',
            as: 'tarjetObjs'
          }},
          {$lookup:{
            from: 'users',
            localField: 'manager',
            foreignField: '_id',
            as: 'managerObjs'
          }},
          {$lookup:{
            from: 'users',
            localField: 'owner',
            foreignField: '_id',
            as: 'ownerObjs'
          }}
        ]).then(proyects=>{
          res.json({
              message: res.__('ok'),
              error: false,
              objs: proyects
          });
        })
      });
}

function destroy(req, res, next){
  let id = req.params.id;
    Proyect.findByIdAndDelete( id , (err , proyect)=>{
        logger.info('Proyecto con id '+id+" Eliminada...");
        // res.json({
        //     message: res.__('ok'),
        //     error: false,
        //     objs: proyect
        // });
        Proyect.aggregate([
          {$match:{
            _id: mongoose.Types.ObjectId(`${proyect._id}`)
          }},
          {$lookup:{
            from: 'users',
            localField: 'equip',
            foreignField: '_id',
            as: 'userObjs'
          }},
          {$lookup:{
            from: 'cards',
            localField: 'history_user_cards',
            foreignField: '_id',
            as: 'tarjetObjs'
          }},
          {$lookup:{
            from: 'users',
            localField: 'manager',
            foreignField: '_id',
            as: 'managerObjs'
          }},
          {$lookup:{
            from: 'users',
            localField: 'owner',
            foreignField: '_id',
            as: 'ownerObjs'
          }}
        ]).then(proyects=>{
          res.json({
              message: res.__('ok'),
              error: false,
              objs: proyects
          });
        })
      });
}

module.exports = {
  index, list, create, update, destroy
}
