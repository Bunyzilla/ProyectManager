const async = require('async');
const bcrypt = require('bcrypt');
const User = require('./../models/user.schema');
const jwt = require('jsonwebtoken');
var serverConf = require('./../config/config');

//Se importa el logger
const logger = require('./../utils/logger');

const jwtKey = serverConf.key;

module.exports = {
  index: (req, res, next)=>{
    res.sendFile(`/dist/index.html`);
  }
};
