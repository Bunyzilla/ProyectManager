/* eslint-disable no-unused-vars */
const async = require('async');
const bcrypt = require('bcrypt');
const User = require('./../models/user.schema');
const jwt = require('jsonwebtoken');
var serverConf = require('./../config/config');

//Se importa el logger
const logger = require('./../utils/logger');

const jwtKey = serverConf.key;

module.exports = {
  login: (req, res, next)=>{
    async.parallel({
      user: (callback) =>{
        User.findOne({ email: req.body.email })
            .select('_password _salt')
            .exec(callback)
      }
    }, (err, result)=>{
      if(result.user){
        bcrypt.hash(req.body.password, result.user.salt, (err, hash)=>{
          if(hash === result.user.password){
            let token = jwt.sign(result.user.id, jwtKey);
            logger.info('Enviando el usuario con id...'+result.user.id);
            res.status(200).json({
                message: res.__('ok'),
                error: true,
                token: token
            });
          }else{
            res.status(200).json({
                message: 'Cuerpo de peticion incorreco.',
                error: true
            });
          }
        });
      }else{
        res.status(200).json({
            message: 'Cuerpo de peticion incorreco.',
            error: true
        });
      }
    });
  }
};
