const Role = require('../models/roles.schema');
//Se importa el logger
const logger = require('../utils/logger');

function list(req, res, next){
    Role.find({}).then(roles => {
        logger.info(`Enviando todos los roles...`);
        res.json({
            message: res.__('ok'),
            error: false,
            objs: roles
        })
    });
}

function index(req, res, next){
  let id = req.params.id;
  Role.findById( id , (err , role)=>{
    logger.info('Enviando el role con id...'+id);
    res.json({
        message: res.__('ok'),
        error: false,
        objs: role
    });
  });
}

function create(req, res, next){
  let role = req.body;

  let newrole = new Role(role);

  newrole.save()
    .then((obj)=>{
        logger.info('Role con id '+obj._id+" Creada...");
        res.status(200).json({
            message: res.__('ok'),
            error: false,
            objs: obj
        });
    })
    .catch((err)=>{
        res.status(200).json({
            message: res.__('error'),
            error: true,
            objs: err
        });
    });
}

function update(req, res, next){
    let newroleData = req.body;
    let id = req.params.id;
    Role.findByIdAndUpdate( id , newroleData , (err , role)=>{
        logger.info('Role con id '+id+' Actualizado...');
        res.json({
            message: res.__('ok'),
            error: false,
            objs: role
        });
      });
}

function destroy(req, res, next){
  let id = req.params.id;
    Role.findByIdAndDelete( id , (err , role)=>{
        logger.info('Role con id '+id+" Eliminado...");
        res.json({
            message: res.__('ok'),
            error: false,
            objs: role
        });
      });
}

module.exports = {
  index, list, create, update, destroy
}
