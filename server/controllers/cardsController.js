const Card = require('../models/card.schema');
//Se importa el logger
const logger = require('../utils/logger');
const mongoose = require('mongoose');



function list(req, res, next){
    // Card.find({}).then(cards => {
    //     logger.info(`Enviando lista de tarjetas...`);
    //     res.json({
    //         message: res.__('ok'),
    //         error: false,
    //         objs: cards
    //     })
    // });
    Card.aggregate([
      {$lookup:{
        from: 'users',
        localField: 'belongs_to_user',
        foreignField: '_id',
        as: 'userObjs'
      }},
      {$lookup:{
        from: 'cardLists',
        localField: 'belongs_to_list',
        foreignField: '_id',
        as: 'cardListObjs'
      }}
    ]).then(cards=>{
      res.json({
          message: res.__('ok'),
          error: false,
          objs: cards
      });
    })
}


function index(req, res, next){
  let id = req.params.id;
  // Card.findById( id , (err , card)=>{
  //   logger.info('Enviando la tarjeta con id...'+id);
  //   res.json({
  //       message: res.__('ok'),
  //       error: false,
  //       objs: card
  //   });
  // });
  Card.aggregate([
    {$match:{
      _id: mongoose.Types.ObjectId(`${id}`)
    }},
    {$lookup:{
      from: 'users',
      localField: 'belongs_to_user',
      foreignField: '_id',
      as: 'userObjs'
    }},
    {$lookup:{
      from: 'cardLists',
      localField: 'belongs_to_list',
      foreignField: '_id',
      as: 'cardListObjs'
    }}
  ]).then(cards=>{
    res.json({
        message: res.__('ok'),
        error: false,
        objs: cards
    });
  })
}

function create(req, res, next){
  let card = req.body;

  let newCard = new User(card);

  newCard.save()
    .then((obj)=>{
        logger.info('Tarjeta con id '+obj._id+" Creada...");
        // res.status(200).json({
        //     message: res.__('ok'),
        //     error: false,
        //     objs: obj
        // });
        Card.aggregate([
          {$match:{
            _id: mongoose.Types.ObjectId(`${obj._id}`)
          }},
          {$lookup:{
            from: 'users',
            localField: 'belongs_to_user',
            foreignField: '_id',
            as: 'userObjs'
          }},
          {$lookup:{
            from: 'cardLists',
            localField: 'belongs_to_list',
            foreignField: '_id',
            as: 'cardListObjs'
          }}
        ]).then(cards=>{
          res.json({
              message: res.__('ok'),
              error: false,
              objs: cards
          });
        })
    })
    .catch((err)=>{
        res.status(200).json({
            message: res.__('error'),
            error: true,
            objs: err
        });
    });
}

function update(req, res, next){
    let newCardData = req.body;
    let id = req.params.id;
    Card.findByIdAndUpdate( id , newCardData , (err , card)=>{
        logger.info('Tarjeta con id '+id+' Actualizada...');
        // res.json({
        //     message: res.__('ok'),
        //     error: false,
        //     objs: card
        // });
        Card.aggregate([
          {$match:{
            _id: mongoose.Types.ObjectId(`${card._id}`)
          }},
          {$lookup:{
            from: 'users',
            localField: 'belongs_to_user',
            foreignField: '_id',
            as: 'userObjs'
          }},
          {$lookup:{
            from: 'cardLists',
            localField: 'belongs_to_list',
            foreignField: '_id',
            as: 'cardListObjs'
          }}
        ]).then(cards=>{
          res.json({
              message: res.__('ok'),
              error: false,
              objs: cards
          });
        })
      });
}

function destroy(req, res, next){
  let id = req.params.id;
    Card.findByIdAndDelete( id , (err , card)=>{
        logger.info('Tarjeta con id '+id+" Eliminada...");
        // res.json({
        //     message: res.__('ok'),
        //     error: false,
        //     objs: card
        // });
        Card.aggregate([
          {$match:{
            _id: mongoose.Types.ObjectId(`${card._id}`)
          }},
          {$lookup:{
            from: 'users',
            localField: 'belongs_to_user',
            foreignField: '_id',
            as: 'userObjs'
          }},
          {$lookup:{
            from: 'cardLists',
            localField: 'belongs_to_list',
            foreignField: '_id',
            as: 'cardListObjs'
          }}
        ]).then(cards=>{
          res.json({
              message: res.__('ok'),
              error: false,
              objs: cards
          });
        })
      });
}

module.exports = {
  index, list, create, update, destroy
}
