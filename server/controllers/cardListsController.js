const CardList = require('../models/cardList.schema');
//Se importa el logger
const logger = require('../utils/logger');

function list(req, res, next){
    CardList.find({}).then(cardLists => {
        logger.info(`Enviando listas de tarjetas...`);
        res.json({
            message: res.__('ok'),
            error: false,
            objs: cardLists
        })
    });
}

function index(req, res, next){
  let id = req.params.id;
  CardList.findById( id , (err , cardList)=>{
    logger.info('Enviando la lista de tarjetas con id...'+id);
    res.json({
        message: res.__('ok'),
        error: false,
        objs: cardList
    });
  });
}

function create(req, res, next){
  let cardList = req.body;

  let newcardList = new CardList(cardList);

  newcardList.save()
    .then((obj)=>{
        logger.info('Lista de tarjetas con id '+obj._id+" Creada...");
        res.status(200).json({
            message: res.__('ok'),
            error: false,
            objs: obj
        });
    })
    .catch((err)=>{
        res.status(200).json({
            message: res.__('error'),
            error: true,
            objs: err
        });
    });
}

function update(req, res, next){
    let newcardListData = req.body;
    let id = req.params.id;
    CardList.findByIdAndUpdate( id , newcardListData , (err , cardList)=>{
        logger.info('Lista de tarjetas con id '+id+' Actualizada...');
        res.json({
            message: res.__('ok'),
            error: false,
            objs: cardList
        });
      });
}

function destroy(req, res, next){
  let id = req.params.id;
    CardList.findByIdAndDelete( id , (err , cardList)=>{
        logger.info('Lista de tarjetas con id '+id+" Eliminada...");
        res.json({
            message: res.__('ok'),
            error: false,
            objs: cardList
        });
      });
}

module.exports = {
  index, list, create, update, destroy
}
