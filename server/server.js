/* eslint-disable no-unused-vars */
//Configuraciones para el servidor
var serverConf = require('./config/config');
var express = require('express');
const expressJwt = require('express-jwt');
const cookieParser = require('cookie-parser');
const i18n = require ('i18n');
const path = __dirname + '/views/';
//Se crea el servidor
var app = express();
var server = require('http').Server(app);

var bodyParser = require('body-parser');
var cors = require('cors');
var mongoose = require('mongoose');
const User = require('./models/user.schema');
const CardList = require('./models/cardList.schema');
//Se importa el logger
const logger = require('./utils/logger');

/////////////////////////////////////////////////////////////////////////////////////

//Se levanta la coneccion a la bdd
mongoose.connect(`mongodb://${serverConf.uri}:27017/scrumbdd`/*, { autoIndex: false }*/).then(
    () => {
        User.find( { email : "admin@admin.com" }, (err,docs)=>{
            if(docs.length == 0){
                new User({
                    email : "admin@admin.com",
                    user : "admin",
                    password : "QWERasdf"
                }).save((err , doc)=>{
                    logger.info('Inicializacion del usuario Admin completa');
                })
            }
            //logger.info('Database connection is successful');
        });
        //todo: add 3 cardlist product, sprint y release (completado)
        CardList.find({ name : "product"} , (err,docs)=>{
          if(docs.length == 0){
              new CardList({
                  name: "product",
                  comment : ""
              }).save((err , doc)=>{
                  logger.info('Inicializacion de la lista product completa');
              })
          }
          //logger.info('Database connection is successful');
        });

        CardList.find({ name : "sprint"} , (err,docs)=>{
          if(docs.length == 0){
              new CardList({
                  name: "sprint",
                  comment : ""
              }).save((err , doc)=>{
                  logger.info('Inicializacion de la lista sprint completa');
              })
          }
          //logger.info('Database connection is successful');
        });

        CardList.find({ name : "release"} , (err,docs)=>{
          if(docs.length == 0){
              new CardList({
                  name: "release",
                  comment : ""
              }).save((err , doc)=>{
                  logger.info('Inicializacion de la lista release completa');
              })
          }
          //logger.info('Database connection is successful');
        });
        //console.log(process.env.NODE_ENV)
    },
    err => { logger.error('Error when connecting to the database' + err) }
);


app.use(bodyParser.urlencoded({ extended: false }));
//Le indicamos que las respuestas se deben de mandar en formato JSON
app.use(bodyParser.json());
//Se habilitan las CORS para que no existan errores de cominicacion
//Se habilitan las CORS para que no existan errores de cominicacion
app.use(cors());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

//Rutas
//var router = express.Router();

//const pageRouter = require('./routes/page')
const loginRouter = require('./routes/login');
const usersRouter = require('./routes/users');
const cardsRouter = require('./routes/cards');
const cardListsRouter = require('./routes/cardLists');
const proyectsRouter = require('./routes/proyects');
const rolesRouter = require('./routes/roles');
//console.log(process.env.NODE_ENV);
const jwtKey = serverConf.key;

//Se implementa el jwt en todas las rutas menos en las
//Rutas desprotegidas por el jwt
 app.use(expressJwt({secret:jwtKey})
  .unless({path: ["/login","/users/register"]}));


app.use(cookieParser());
i18n.configure({
   locales:['en','es'],
   cookie:'language',
   directory:__dirname+'/locales'
});
app.use(i18n.init);
app.use(express.static(path));
//app.use('/',router);
app.use('/', loginRouter);
app.use('/users',usersRouter);
app.use('/cards',cardsRouter);
app.use('/cardLists',cardListsRouter);
app.use('/proyects',proyectsRouter);
app.use('/roles',rolesRouter);


//app.use('/', router);

//Metodo para lanzar la app por el puerto
server.listen(serverConf.port, () => {
    logger.info(`Server running on port ${serverConf.port} ...`);
});

module.exports = server;
