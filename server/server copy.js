//Configuraciones para el servidor
var serverConf = require('./config/config');
var express = require('express');
//Se crea el servidor
var app = express();
var server = require('http').Server(app);

var bodyParser = require('body-parser');
var cors = require('cors');
var mongoose = require('mongoose');

//Se importa el logger 
const logger = require('./utils/logger');

/////////////////////////////////////////////////////////////////////////////////////

//Se levanta la coneccion a la bdd
mongoose.connect("mongodb://localhost:27017/scrumbdd"/*, { autoIndex: false }*/).then(
    () => { logger.info('Database connection is successful');console.log() },
    err => { logger.error('Error when connecting to the database' + err) }
);


app.use(bodyParser.urlencoded({ extended: false }));
//Le indicamos que las respuestas se deben de mandar en formato JSON
app.use(bodyParser.json());
//Se habilitan las CORS para que no existan errores de cominicacion 
app.use(cors());


//Schemas para el manejo de las colecciones
const Todo = require('./models/Todo.schema');
//Ayuda para controlar el valor del index aunque no era requerido
const indexController = require('./utils/indexes');
//Controlador para enviar la respuesta del servidor
const responsesController = require('./utils/ServerResponse')
//Controlador que me ayuda a guardar el historial de cambios en la bdd
const historicController = require('./utils/historic');
//Rutas
var router = express.Router();

//Creating a Todo: Express.js POST Route Example

router.route('/create').post((req, res) => {
    //Obteniendo el index actual de los todos
    indexController.getActualIndex(0)
    .then((actualIndex) => {
        req.body._id = actualIndex.current_id;
        var todo = new Todo(req.body);
        todo.save()
        .then(todo => {
            historicController.logMovements(todo,0).then((response)=>{
                const messageResponse = responsesController.returnSuccessMessage(response,todo);
                logger.debug(messageResponse.message);
                res.status(200).json(messageResponse);
            }).catch(err => {
                const messageResponse = responsesController.returnErrorMessage(
                    err.errors.type.message,{});
                logger.error(messageResponse.message);
                res.status(400).send(messageResponse);
            });
        })
        .catch(err => {
            const messageResponse = responsesController.returnSuccessMessage(
                err.errors.type.message,{});
            logger.error(messageResponse.message);
            res.status(400).send(messageResponse);
        });
    });
    
});

//Getting Todos: Express.js GET Route Example

router.route('/todos').get((req, res) => {
    Todo.find({ deleted : false },(err, todos) => {
        if (err) {
            const messageResponse = responsesController.returnErrorMessage(
                "Error in search for todos",{});
            logger.error(messageResponse.message);
            res.status(400).send(messageResponse);
        }
        else {
            const messageResponse = responsesController.returnSuccessMessage(
                "Todos search success",todos);
            logger.debug(messageResponse.message);
            res.status(200).json(messageResponse);
        }
    });
});

router.route('/todos-page/:num').get((req, res) => {
    Todo.paginate({ deleted : false }, { page: req.params.num, limit: 10 }, (err, result) => {
        if(err) { 
            const messageResponse = responsesController.returnErrorMessage(
                "Error in search for todos",{});
            logger.error(messageResponse.message);
            res.status(500).json(messageResponse); return; 
        };
        const messageResponse = responsesController.returnSuccessMessage(
            "Todos search success",result);
        logger.debug(messageResponse.message);
        res.status(200).json(messageResponse);
    });
});

//Getting a Todo by Id: Express.js GET by Id Route Example
router.route('/todos/:id').get((req, res) => {
    var id = req.params.id;
    Todo.findById(id, (err, todo) => {
        if(err) { 
            const messageResponse = responsesController.returnErrorMessage(
                "Error in search for todo " + id,{});
            logger.error(messageResponse.message);
            res.status(500).json(messageResponse); return; 
        };
        const messageResponse = responsesController.returnSuccessMessage(
            "Todo " + id + " search success",todo);
        logger.debug(messageResponse.message);
        res.status(200).json(messageResponse);
    });
});

//Updating a Todo by Id: Express.js PUT Route Example
router.route('/todos/:id').put((req, res) => {
    Todo.findById(req.params.id, (err, todo) => {
        if (!todo){
            const messageResponse = responsesController.returnErrorMessage(
                "Error in search for todo " + id,{});
            logger.error("Error in search for todo " + id);
            return next(new Error(messageResponse))
        } else {
            let description = '';
            //Se verifica que existan cambios si no no se actualiza el todo
            //en base a los cambios recibidos se crea una descripcion
            if(todo.name != req.body.name || todo.description != req.body.description){
                description = 'Todo #'+todo._id+' ';
                if(todo.name != req.body.name){
                    description = description + ' name changing from "'+todo.name+'" to "'+req.body.name+'"';
                    todo.name = req.body.name;
                }
                if(todo.name != req.body.name && todo.description != req.body.description){
                    description = description + ' and ';
                }
                if(todo.description != req.body.description){
                    description = description + ' description changing from "'+todo.description+'" to "'+req.body.description+'"';
                    todo.description = req.body.description;
                }
                todo.save()
                .then(todoUpdated => {
                    historicController.logMovements(todoUpdated,1,description).then((response)=>{
                        const messageResponse = responsesController.returnSuccessMessage(response,todo);
                        logger.debug(messageResponse.message);;
                        res.status(200).json(messageResponse);
                    }).catch(err => {
                        const messageResponse = responsesController.returnErrorMessage(
                            err.errors.type.message,{});
                        logger.error(err.errors.type.message);    
                        res.status(400).send(messageResponse);
                    });
                })
                .catch(err => {
                    const messageResponse = responsesController.returnErrorMessage(
                        err.errors.type.message,{});
                    logger.error(err.errors.type.message);
                    res.status(400).send(messageResponse);
                });
            }
        }
    });
});

//Deleting a Todo by Id: Express.js DELETE Route Example
router.route('/todos/:id').delete((req, res) => {
    //Para hacer un borrado logico se debe tener un campo donde nos indique si este esta activo o no
    Todo.findByIdAndUpdate({ _id: req.params.id }, { deleted: true }, (err, todo) => {
        historicController.logMovements(todo,2).then((response)=>{
            const messageResponse = responsesController.returnSuccessMessage(
                response,todo);
            logger.debug(messageResponse.message);
            res.status(200).json(messageResponse);
        }).catch(err => {
            const messageResponse = responsesController.returnErrorMessage(
                "Error deleting todo "+ req.params.id + " : "+err,{});
            logger.error(messageResponse.message);
            res.status(400).send(messageResponse);
        });
    });
});

//Geting historic todos with page and type options
router.route('/historic-todos-page/:num/:type').get((req, res) => {
    historicController.getHistoric(req.params.num , req.params.type)
    .then(result => {
        const messageResponse = responsesController.returnSuccessMessage(
            "Todos historic  search success",result);
        logger.debug(messageResponse.message);
        res.status(200).json(messageResponse);
    })
    .catch(err => {
        const messageResponse = responsesController.returnErrorMessage(
            "Error in search for historic todos",{});
        logger.error(messageResponse.message);
        res.status(500).json(messageResponse); 
    });
});


app.use('/', router);

//Metodo para lanzar la app por el puerto
server.listen(serverConf.port, () => {
    logger.info(`Server running on port ${serverConf.port} ...`);
});
