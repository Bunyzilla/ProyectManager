const express = require('express');
const router = express.Router();
const controller = require('../controllers/rolesController');

//obtener todos los roles
router.get('/', controller.list);
//Obtener un rol por id
router.get('/index/:id', controller.index);
//Crear un rol
router.post('/', controller.create);
//Actualizar un rol por id
router.put('/:id', controller.update);
//Eliminar un rol por id
router.delete('/:id', controller.destroy);

module.exports = router;