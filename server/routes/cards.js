const express = require('express');
const router = express.Router();
const controller = require('../controllers/cardsController');

//obtener todos 
router.get('/', controller.list);
//Obtener por id
router.get('/index/:id', controller.index);
//Crear 
router.post('/', controller.create);
//Actualizar un usuario por id
router.put('/:id', controller.update);
//Eliminar un usuario
router.delete('/:id', controller.destroy);

module.exports = router;