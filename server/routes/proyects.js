const express = require('express');
const router = express.Router();
const controller = require('../controllers/proyectsController');

//obtener todos los proyectos
router.get('/', controller.list);
//Obtener un proyecto por id
router.get('/index/:id', controller.index);
//Crear un proyecto
router.post('/', controller.create);
//Actualizar un proyecto por id
router.put('/:id', controller.update);
//Eliminar un proyecto por id
router.delete('/:id', controller.destroy);

module.exports = router;