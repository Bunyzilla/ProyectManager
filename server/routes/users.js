const express = require('express');
const router = express.Router();
const controller = require('../controllers/usersController');

//obtener todos los usuarios
router.get('/', controller.list);
//Obtener un usuario por id
router.get('/index/:id', controller.index);
//todo: agregar ruta: buscar id de usuario dentro de owner, manager y equip
//y regresar todos los proyectos a los que pertenezca (completado)
router.get('/proyects/:id', controller.listProyects);
//Crear un usuario
router.post('/register', controller.create);
//Actualizar un usuario
router.put('/:id', controller.update);
//Eliminar un usuario
router.delete('/:id', controller.destroy);


module.exports = router;
