/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
//import CreateTodo from '../../components/CreateTodo/CreateTodo.vue';
//import ListTodo from '../../components/ListTodo/ListTodo.vue';
//import { apiService } from '../../store/apiServie.js';
import LogedHeader from '../../components/headers/LogedHeader/LogedHeader.vue';
import LeftSideBar from '../../components/sidebars/LeftSideBar/LeftSideBar.vue';

const importsView = require('../../utils/importsTemplates').listas;

export default {
	name: 'listas',
	components: {
		//CreateTodo,
		//ListTodo
		LogedHeader,
		LeftSideBar
	},
	data() {
		return {
			headImportsOps : importsView.headImportsOps,
			bodyImportsOps : importsView.bodyImportsOps
		};
	},
	mounted() {
		//Se levanta una coneccion con el soket en este componente
		/*apiService.state.socket.on('messages', (data) => {
			//console.warn(data);
			this.$swal(data.toUpperCase());
		});*/
		this.setHeadImports().then(()=>{
			this.setBodyImports().then(()=>{
				console.log('simon');
			})
		})
	},
	methods: {
		setHeadImports(){
			return new Promise((resolve , reject)=>{
				this.headImportsOps.forEach(element => {
					let script = document.createElement(element.type);    
					element.setAttributes.forEach(atribute =>{
						script.setAttribute(atribute.name,atribute.opc);
						document.head.appendChild(script);
					});
					resolve();
				});
			})
		},
		setBodyImports(){
			return new Promise((resolve , reject)=>{
				this.bodyImportsOps.forEach(element => {
					let script = document.createElement(element.type);    
					element.setAttributes.forEach(atribute =>{
						script.setAttribute(atribute.name,atribute.opc);
						document.body.appendChild(script);
					});
					resolve();
				});
			})
		},
	}
}