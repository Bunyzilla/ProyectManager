/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
//import CreateTodo from '../../components/CreateTodo/CreateTodo.vue';
//import ListTodo from '../../components/ListTodo/ListTodo.vue';
//import { apiService } from '../../store/apiServie.js';
import PublicHeader2 from '../../components/headers/PublicHeader2/PublicHeader2.vue';
import PublicFooter from '../../components/footers/PublicFooter/PublicFooter.vue';

const importsView = require('../../utils/importsTemplates').about;

export default {
	name: 'about',
	components: {
		//CreateTodo,
		//ListTodo,
		PublicHeader2,
		PublicFooter
	},
	data() {
		return {
			headImportsOps : importsView.headImportsOps,
			bodyImportsOps : importsView.bodyImportsOps
		};
	},
	mounted() {
		//Se levanta una coneccion con el soket en este componente
		/*apiService.state.socket.on('messages', (data) => {
			//console.warn(data);
			this.$swal(data.toUpperCase());
		});*/
		this.setHeadImports().then(()=>{
			this.setBodyImports().then(()=>{
				console.log('simon');
			})
		})
	},
	methods: {
		setHeadImports(){
			return new Promise((resolve , reject)=>{
				this.headImportsOps.forEach(element => {
					let script = document.createElement(element.type);    
					element.setAttributes.forEach(atribute =>{
						script.setAttribute(atribute.name,atribute.opc);
						document.head.appendChild(script);
					});
					resolve();
				});
			})
		},
		setBodyImports(){
			return new Promise((resolve , reject)=>{
				this.bodyImportsOps.forEach(element => {
					let script = document.createElement(element.type);    
					element.setAttributes.forEach(atribute =>{
						script.setAttribute(atribute.name,atribute.opc);
						document.body.appendChild(script);
					});
					resolve();
				});
			})
		},
	}
}