/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
//import CreateTodo from '../../components/CreateTodo/CreateTodo.vue';
//import ListTodo from '../../components/ListTodo/ListTodo.vue';
import { apiService } from '../../store/apiServie.js';
const importsView = require('../../utils/importsTemplates').index;

export default {
	name: 'login',
	components: {
		//CreateTodo,
		//ListTodo
	},
	data() {
		return {
			headImportsOps : importsView.headImportsOps,
			bodyImportsOps : importsView.bodyImportsOps
		};
	},
	async created() {
		await this.setHeadImports().then(async ()=>{
			await this.setBodyImports().then(()=>{
				console.log('simon');
			})
		})
	},
	mounted() {
		//Se levanta una coneccion con el soket en este componente
		/*apiService.state.socket.on('messages', (data) => {
			//console.warn(data);
			this.$swal(data.toUpperCase());
		});*/
		apiService.login('QWERasdf', 'admin@admin.com')
			.then(()=>{
				console.log('Login correcto');
			})
		
	},
	methods: {
		setHeadImports(){
			return new Promise(async (resolve , reject)=>{
				await this.headImportsOps.forEach(element => {
					let script = document.createElement(element.type);    
					element.setAttributes.forEach(atribute =>{
						script.setAttribute(atribute.name,atribute.opc);
						document.head.appendChild(script);
					});
					resolve();
				});
			})
		},
		setBodyImports(){
			return new Promise((resolve , reject)=>{
				this.bodyImportsOps.forEach(element => {
					let script = document.createElement(element.type);    
					element.setAttributes.forEach(atribute =>{
						script.setAttribute(atribute.name,atribute.opc);
						document.body.appendChild(script);
					});
					resolve();
				});
			})
		},
	}
}