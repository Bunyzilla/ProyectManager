/* eslint-disable indent */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
//import CreateTodo from '../../components/CreateTodo/CreateTodo.vue';
//import ListTodo from '../../components/ListTodo/ListTodo.vue';
//import { apiService } from '../../store/apiServie.js';
import LogedHeader from '../../components/headers/LogedHeader/LogedHeader.vue';
import LeftSideBar2 from '../../components/sidebars/LeftSideBar2/LeftSideBar2.vue';
import { ModelSelect } from 'vue-search-select';
import Dropdown from 'vue-simple-search-dropdown';

const importsView = require('../../utils/importsTemplates').proyectos;
import Multiselect from 'vue-multiselect';

export default {
    name: 'proyectos',
    components: {
        //CreateTodo,
        //ListTodo
        LogedHeader,
        LeftSideBar2,
        Multiselect
    },
    data() {
        return {
            headImportsOps: importsView.headImportsOps,
            bodyImportsOps: importsView.bodyImportsOps,
            value: { name: 'Vue.js', language: 'JavaScript' },
            options: [
                { name: 'Vue.js', language: 'JavaScript' },
                { name: 'Rails', language: 'Ruby' },
                { name: 'Sinatra', language: 'Ruby' },
                { name: 'Laravel', language: 'PHP' },
                { name: 'Phoenix', language: 'Elixir' }
            ]
        };
    },
    mounted() {
        //Se levanta una coneccion con el soket en este componente
        /*apiService.state.socket.on('messages', (data) => {
        	//console.warn(data);
        	this.$swal(data.toUpperCase());
        });*/
        this.setHeadImports().then(() => {
            this.setBodyImports().then(() => {
                console.log('simon');
            })
        })
    },
    methods: {
        nameWithLang({ name, language }) {
            return `${name} — [${language}]`
        },
        setHeadImports() {
            return new Promise((resolve, reject) => {
                this.headImportsOps.forEach(element => {
                    let script = document.createElement(element.type);
                    element.setAttributes.forEach(atribute => {
                        script.setAttribute(atribute.name, atribute.opc);
                        document.head.appendChild(script);
                    });
                    resolve();
                });
            })
        },
        setBodyImports() {
            return new Promise((resolve, reject) => {
                this.bodyImportsOps.forEach(element => {
                    let script = document.createElement(element.type);
                    element.setAttributes.forEach(atribute => {
                        script.setAttribute(atribute.name, atribute.opc);
                        document.body.appendChild(script);
                    });
                    resolve();
                });
            })
        },
    }
}