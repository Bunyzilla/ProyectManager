/* eslint-disable indent */
const index = {
    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'href',
                    opc: 'https://image.flaticon.com/icons/svg/929/929339.svg'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/bootstrap/css/bootstrap.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                },
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                },
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'assets/fonts/iconic/css/material-design-iconic-font.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/animate/animate.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/css-hamburgers/hamburgers.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/animsition/css/animsition.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/select2/select2.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/daterangepicker/daterangepicker.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'assets/css/util.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'type',
                    opc: 'text/css'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main.css'
                }
            ]
        }
        /*	Esto casi siempre esta en el head despues de los meta
         *<link rel="icon" type="image/png" sizes="96x96" href="https://image.flaticon.com/icons/svg/929/929339.svg">
         *<link rel="stylesheet" type="text/css" href="assets/vendor/bootstrap/css/bootstrap.min.css">
         *<link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
         *<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">
         *<link href="https://fonts.googleapis.com/css?family=Slabo+27px&display=swap" rel="stylesheet">
         *<link rel="stylesheet" type="text/css" href="assets/fonts/iconic/css/material-design-iconic-font.min.css">
         *<link rel="stylesheet" type="text/css" href="assets/vendor/animate/animate.css">
         *<link rel="stylesheet" type="text/css" href="assets/vendor/css-hamburgers/hamburgers.min.css">
         *<link rel="stylesheet" type="text/css" href="assets/vendor/animsition/css/animsition.min.css">
         *<link rel="stylesheet" type="text/css" href="assets/vendor/select2/select2.min.css">
         *<link rel="stylesheet" type="text/css" href="assets/vendor/daterangepicker/daterangepicker.css">
         *<link rel="stylesheet" type="text/css" href="assets/css/util.css">
         *<link rel="stylesheet" type="text/css" href="assets/css/main.css">*/
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/jquery/jquery-3.2.1.min.js'
            }, ]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/animsition/js/animsition.min.js'
            }, ]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/bootstrap/js/popper.js'
            }, ]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/bootstrap/js/bootstrap.min.js'
            }, ]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/select2/select2.min.js'
            }, ]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/daterangepicker/moment.min.js'
            }, ]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/daterangepicker/daterangepicker.js'
            }, ]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/countdowntime/countdowntime.js'
            }, ]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/js/main.js'
            }, ]
        },
        /* Esto casi siempre esta en el body mero abajo
         *<script src="assets/vendor/jquery/jquery-3.2.1.min.js"></script>
         *<script src="assets/vendor/animsition/js/animsition.min.js"></script>
         *<script src="assets/vendor/bootstrap/js/popper.js"></script>
         *<script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>
         *<script src="assets/vendor/select2/select2.min.js"></script>
         *<script src="assets/vendor/daterangepicker/moment.min.js"></script>
         *<script src="assets/vendor/daterangepicker/daterangepicker.js"></script>
         *<script src="assets/vendor/countdowntime/countdowntime.js"></script>
         *<script src="assets/js/main.js"></script>*/
    ],
};

const about = {
    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main1.css' // esta originalmente era esto ./css/main1.css
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css'
                }
            ]
        }
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'https://code.jquery.com/jquery-3.3.1.slim.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'
            }]
        }
    ]
};

const actividades = {
    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/bootstrap/css/bootstrap.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/font-awesome/css/font-awesome.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/linearicons/style.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main3.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/demo.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'apple-touch-icon'
                },
                {
                    name: 'sizes',
                    opc: '76x76'
                },
                {
                    name: 'href',
                    opc: 'assets/images/apple-icon.png'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'sizes',
                    opc: '96x96'
                },
                {
                    name: 'href',
                    opc: 'https://image.flaticon.com/icons/svg/929/929339.svg'
                }
            ]
        },
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery/jquery.min.js'
            }]
        }, {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/bootstrap/js/bootstrap.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'
            }]
        },

        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/js/klorofil-common.js'
            }]
        }
    ]
};

const ajustes = {
    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/bootstrap/css/bootstrap.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/font-awesome/css/font-awesome.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/linearicons/style.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/toastr/toastr.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main3.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/demo.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'apple-touch-icon'
                },
                {
                    name: 'sizes',
                    opc: '76x76'
                },
                {
                    name: 'href',
                    opc: 'assets/images/apple-icon.png'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'sizes',
                    opc: '96x96'
                },
                {
                    name: 'href',
                    opc: 'https://image.flaticon.com/icons/svg/929/929339.svg'
                }
            ]
        },
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery/jquery.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/bootstrap/js/bootstrap.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/toastr/toastr.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/js/klorofil-common.js'
            }]
        }
    ]
};

const board = {
    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/bootstrap/css/bootstrap.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/font-awesome/css/font-awesome.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/linearicons/style.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/chartist/css/chartist-custom.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main3.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/demo.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'apple-touch-icon'
                },
                {
                    name: 'sizes',
                    opc: '76x76'
                },
                {
                    name: 'href',
                    opc: 'assets/images/apple-icon.png'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'sizes',
                    opc: '96x96'
                },
                {
                    name: 'href',
                    opc: 'https://image.flaticon.com/icons/svg/929/929339.svg'
                }
            ]
        },
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery/jquery.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/bootstrap/js/bootstrap.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/chartist/js/chartist.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/js/klorofil-common.js'
            }]
        }
    ]
};

const listas = {

    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/bootstrap/css/bootstrap.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/font-awesome/css/font-awesome.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/linearicons/style.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main3.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/demo.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'apple-touch-icon'
                },
                {
                    name: 'sizes',
                    opc: '76x76'
                },
                {
                    name: 'href',
                    opc: 'assets/images/apple-icon.png'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'sizes',
                    opc: '96x96'
                },
                {
                    name: 'href',
                    opc: 'https://image.flaticon.com/icons/svg/929/929339.svg'
                }
            ]
        },
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery/jquery.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/bootstrap/js/bootstrap.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/js/klorofil-common.js'
            }]
        }
    ]
};
const mainIndex = {
    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                name: 'href',
                opc: 'https://code.jquery.com/jquery-3.3.1.slim.min.js'
            }]
        },
        {
            type: 'link',
            setAttributes: [{
                name: 'href',
                opc: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'
            }]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main1.css' // originalmente era esto ./css/main1.css
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css'
                }
            ]
        }
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'https://code.jquery.com/jquery-3.3.1.slim.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'
            }]
        }
    ]
};
const pageProfile = {

    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/bootstrap/css/bootstrap.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/font-awesome/css/font-awesome.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/linearicons/style.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main3.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/demo.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'apple-touch-icon'
                },
                {
                    name: 'sizes',
                    opc: '76x76'
                },
                {
                    name: 'href',
                    opc: 'assets/images/apple-icon.png'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'sizes',
                    opc: '96x96'
                },
                {
                    name: 'href',
                    opc: 'https://image.flaticon.com/icons/svg/929/929339.svg'
                }
            ]
        },
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery/jquery.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/bootstrap/js/bootstrap.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/js/klorofil-common.js'
            }]
        }
    ]
};
const proSet = {

    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/bootstrap/css/bootstrap.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/font-awesome/css/font-awesome.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/linearicons/style.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/toastr/toastr.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main3.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/demo.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'apple-touch-icon'
                },
                {
                    name: 'sizes',
                    opc: '76x76'
                },
                {
                    name: 'href',
                    opc: 'assets/images/apple-icon.png'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'sizes',
                    opc: '96x96'
                },
                {
                    name: 'href',
                    opc: 'https://image.flaticon.com/icons/svg/929/929339.svg'
                }
            ]
        },
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery/jquery.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/bootstrap/js/bootstrap.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/toastr/toastr.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/js/klorofil-common.js'
            }]
        }
    ]
};

const proyectos = {

    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/bootstrap/css/bootstrap.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/font-awesome/css/font-awesome.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/linearicons/style.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/chartist/css/chartist-custom.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main3.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/demo.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'apple-touch-icon'
                },
                {
                    name: 'sizes',
                    opc: '76x76'
                },
                {
                    name: 'href',
                    opc: 'assets/images/apple-icon.png'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'sizes',
                    opc: '96x96'
                },
                {
                    name: 'href',
                    opc: 'https://image.flaticon.com/icons/svg/929/929339.svg'
                }
            ]
        },
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery/jquery.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/bootstrap/js/bootstrap.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/chartist/js/chartist.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/js/klorofil-common.js'
            }]
        }
    ]
};

const registro = {
    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'sizes',
                    opc: '96x96'
                },
                {
                    name: 'href',
                    opc: 'https://image.flaticon.com/icons/svg/929/929339.svg'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'href',
                    opc: 'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'assets/vendor/mdi-font/css/material-design-iconic-font.min.css' // originalmente vendor/mdi-font/css/material-design-iconic-font.min.css
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'assets/vendor/font-awesome-4.7/css/font-awesome.min.css' // originalmente vendor/font-awesome-4.7/css/font-awesome.min.css
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'assets/vendor/select2/select2.min.css' // originalmente vendor/select2/select2.min.css
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'media',
                    opc: 'all'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'assets/vendor/datepicker/daterangepicker.css' // originalmente vendor/datepicker/daterangepicker.css
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'media',
                    opc: 'all'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'assets/css/main2.css'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'media',
                    opc: 'all'
                }
            ]
        }
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery/jquery.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/select2/select2.min.js' //originalmente vendor/select2/select2.min.js
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/datepicker/moment.min.js' //originalmente vendor/datepicker/moment.min.js
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/datepicker/daterangepicker.js' //originalmente vendor/datepicker/daterangepicker.js
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/js/global.js'
            }]
        }
    ]
};


const tarjetas = {
    headImportsOps: [{
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/bootstrap/css/bootstrap.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/font-awesome/css/font-awesome.min.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/assets/vendor/linearicons/style.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/vendor/chartist/css/chartist-custom.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/main3.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'stylesheet'
                },
                {
                    name: 'href',
                    opc: 'assets/css/demo.css'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'href',
                    opc: 'https://fonts.googleapis.com/css?family=Slabo+27px&display=swap'
                },
                {
                    name: 'rel',
                    opc: 'stylesheet'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'apple-touch-icon'
                },
                {
                    name: 'sizes',
                    opc: '76x76'
                },
                {
                    name: 'href',
                    opc: 'assets/images/apple-icon.png'
                }
            ]
        },
        {
            type: 'link',
            setAttributes: [{
                    name: 'rel',
                    opc: 'icon'
                },
                {
                    name: 'type',
                    opc: 'image/png'
                },
                {
                    name: 'sizes',
                    opc: '96x96'
                },
                {
                    name: 'href',
                    opc: 'https://image.flaticon.com/icons/svg/929/929339.svg'
                }
            ]
        }
    ],
    bodyImportsOps: [{
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery/jquery.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/bootstrap/js/bootstrap.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/vendor/chartist/js/chartist.min.js'
            }]
        },
        {
            type: 'script',
            setAttributes: [{
                name: 'src',
                opc: 'assets/js/klorofil-common.js'
            }]
        }
    ]
};


module.exports = {
    index,
    about,
    actividades,
    ajustes,
    board,
    listas,
    mainIndex,
    pageProfile,
    proSet,
    proyectos,
    registro,
    tarjetas
    //Aqui se pone una variable por cada vista que nesecite importar
};