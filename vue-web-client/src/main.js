/* eslint-disable indent */
/* eslint-disable quotes */
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';
//Componente o libreria para crear alertas o notificaciones 
import VueSweetalert2 from 'vue-sweetalert2';

// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';

//Importando bootstrap
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import VueRouter from 'vue-router';
//
import VueFlex from 'vue-flex';
import 'vue-flex/dist/vue-flex.css';

//App principal
import App from './App.vue';

//Se le agregan los componentes con use
Vue.use(BootstrapVue);
Vue.use(VueSweetalert2);
Vue.use(VueRouter);
Vue.use(VueFlex);


Vue.config.productionTip = false;

//Paginas

import mainIndex from './pages/main_index/main_index.vue';
import Login from './pages/login/login.vue';
import About from './pages/about/about.vue';
import pageActividades from './pages/actividades/actividades.vue';
import pageAjustes from './pages/ajustes/ajustes.vue';
import pageBoard from './pages/board/board.vue';
import pageListas from './pages/listas/listas.vue';
import pageProfile from './pages/page-profile/page-profile.vue';
import pageProSet from './pages/proSet/proSet.vue';
import pageProyectos from './pages/proyectos/proyectos.vue';
import pageRegistro from './pages/registro/registro.vue';
import pageTarjetas from './pages/tarjetas/tarjetas.vue';
import pageNotfound from './pages/notfound/notfound.vue';
import 'vue-search-select/dist/VueSearchSelect.css';

const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: mainIndex },
        { path: '/login', component: Login },
        { path: '/about', component: About },
        { path: '/actividades', component: pageActividades },
        { path: '/ajustes', component: pageAjustes },
        { path: '/board', component: pageBoard },
        { path: '/listas', component: pageListas },
        { path: '/page-profile', component: pageProfile },
        { path: '/proSet', component: pageProSet },
        { path: '/proyectos', component: pageProyectos },
        { path: '/registro', component: pageRegistro },
        { path: '/tarjetas', component: pageTarjetas },
        // and finally the default route, when none of the above matches:
        { path: '*', component: pageNotfound }
    ]
});


new Vue({
    router,
    render: h => h(App),
}).$mount('#app')