export class CardObj {
	name= '';
	description = '';
	how = '';
	for = '';
	want = '';
	belongs_to_list = '';
	priority = '';
	size = 0;
	validated = true;
	state = '';
	date_created = '';
	belongs_to_user = '';
	userObjs=[{}];
	cardListObjs=[{}];
	create = function(doc) {
		this.name = doc.name;
		this.description = doc.description;
		this.how = doc.how;
		this.for = doc.for;
		this.want = doc.want;
		this.belongs_to_list = doc.belongs_to_list;
		this.priority = doc.priority;
		this.size = doc.size;
		this.validated = doc.validated;
		this.state = doc.state;
		this.date_created = doc.date_created;
		this.belongs_to_user = doc.belongs_to_user;
	}
}
/* module.exports = CardObj; */
