/* eslint-disable no-mixed-spaces-and-tabs */
/* eslint-disable indent */
/* eslint-disable no-undef */
import axios from 'axios';
const API_URL = 'http://localhost:4000';

//import AbilityObj from './userObj';
import UserObj from './userObj';
import CardObj from './cardObj';
import CardListObj from './cardListObj';
import ProyectObj from './proyectObj';
import RoleObj from './roleObj';

//let AbilityObj = require('./userObj').AbilityObj;
//let UserObj = require('./userObj').UserObj;
//const CardObj = require('./cardObj') ;
//let CardListObj = require('./cardListObj') ;
//let ProyectObj = require('./proyectObj') ;
//let RoleObj = require('./roleObj') ;

//const logger = require('../utils/logger');
//import io from 'socket.io-client';
/*
app.use('/', loginRouter);
app.use('/users',usersRouter);
app.use('/cards',cardsRouter);
app.use('/cardLists/',cardListsRouter);
app.use('/proyects',proyectsRouter);
app.use('/roles',rolesRouter);
*/

export const apiService = {
	state: {
		token : '',

		cardObj: [CardObj],

		cardListObj: [CardListObj],

		proyectObj: [ProyectObj],

		roleObj: [RoleObj],

		userObj: [UserObj],

		//abilityObj: AbilityObj

		//Se levanta la coneccion con el soket en el store para hacerlo disponible en la app
		//socket : io.connect('http://localhost:4000', { 'forceNew': true })
	},
	//
	//APPI CRUD
	//////////card
	// //Crear
	// router.post('/', controller.create);

	async createCardObj(data){
		const url = `${API_URL}/cards/`;
		//var out = [];
		await axios.post(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{

			for (obj in response.objs){
				out.push(CardObj.create(obj));
			}
			this.cardObj = out;
			//this. = out;
		});
		//logger.info(out);
		////return out;
	},

	// //Actualizar una tarjeta por id
	// router.put('/:id', controller.update);

	async updateCardObj(data, id){
		const url = `${API_URL}/cards/${id}`;
		var out = [];
		await axios.put(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{

			for (obj in response.objs){
				out.push(CardObj.create(obj));
			}
			this.cardObj = out;
			//this. = out;
		});
		//logger.info(out);
		//return out;
	},
	// //Obtener por id
	// router.get('/index/:id', controller.index);

	async getCardObj(data, id){
		const url = `${API_URL}/cards/index/${id}`;
		var out = [];
		await axios.get(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			for (obj in response.objs){
				out.push(CardObj.create(obj));
			}
			this.cardObj = out;
		});
		//logger.info(out);
		////return out;
	},
	//obtener todos
	// router.get('/', controller.list);

	async listCardObj(){
		const url = `${API_URL}/cards`;
		var out = [];
		await axios.get(url , {
			  headers: {'Authorization': `bearer ${token}`}
		}).then(response => {

			for (obj in response.objs){
				out.push(CardObj.create(obj));
			}
			this.cardObj = out;
		});
		//logger.info(out);
		//return out;
	},

	// //Eliminar una tarjeta
	// router.delete('/:id', controller.destroy);

	async deleteCardObj(id){
		const url = `${API_URL}/cards/index/${id}`;
		//var out = [];
		await axios.delete(url,{
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			// for (obj in response.objs){
			// 	out.push(CardObj.create(obj));
			// }
			// this. = out;
			logger.info(response.message);
		});
		//logger.info(out);
		//return out;
	},

///////////////cardlist
	// //Crear
	// router.post('/', controller.create);

	async createCardListObj(data){
		const url = `${API_URL}/cardLists/`;
		var out = [];
		await axios.post(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{

			for (obj in response.objs){
				out.push(CardListObj.create(obj));
			}
			this.cardListObj = out;
		});
		//logger.info(out);
		//return out;
	},

	// //Actualizar una tarjeta por id
	// router.put('/:id', controller.update);

	async updateCardListObj(data, id){
		const url = `${API_URL}/cardLists/${id}`;
		var out = [];
		await axios.put(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			for (obj in response.objs){
				out.push(CardListObj.create(obj));
			}
			this.cardListObj = out;
		});
		//logger.info(out);
		//return out;
	},
	// //Obtener por id
	// router.get('/index/:id', controller.index);

	async getCardListObj(data, id){
		const url = `${API_URL}/cardLists/index/${id}`;
		var out = [];
		await axios.get(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			for (obj in response.objs){
				out.push(CardListObj.create(obj));
			}
			this.cardListObj = out;
		});
		//logger.info(out);
		//return out;
	},
	//obtener todos
	// router.get('/', controller.list);

	async listCardListObj(){
		const url = `${API_URL}/cards`;
		var out = [];
		await axios.get(url , {
				headers: {'Authorization': `bearer ${token}`}
		}).then(response => {

			for (obj in response.objs){
				out.push(CardListObj.create(obj));
			}
			this.cardListObj = out;
		});
		//logger.info(out);
		//return out;
	},

	// //Eliminar una tarjeta
	// router.delete('/:id', controller.destroy);

	async deleteCardListObj(id){
		const url = `${API_URL}/cardLists/index/${id}`;
		//var out = [];
		await axios.delete(url,{
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			// for (obj in response.objs){
			// 	out.push(CardListObj.create(obj));
			// }
			// this. = out;
			logger.info(response.message);
		});
		//logger.info(out);
		//return out;
	},

	//////proyect
	// //Crear
	// router.post('/', controller.create);

	async createProyect(data){
		const url = `${API_URL}/proyects/`;
		var out = [];
		await axios.post(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{

			for (obj in response.objs){
				out.push(ProyectObj.create(obj));
			}
			this.proyectObj = out;
		});
		//logger.info(out);
		//return out;
	},

	// //Actualizar una tarjeta por id
	// router.put('/:id', controller.update);

	async updateProyect(data, id){
		const url = `${API_URL}/proyects/${id}`;
		var out = [];
		await axios.put(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			for (obj in response.objs){
				out.push(ProyectObj.create(obj));
			}
			this.proyectObj = out;
		});
		//logger.info(out);
		//return out;
	},
	// //Obtener por id
	// router.get('/index/:id', controller.index);

	async getProyect(data, id){
		const url = `${API_URL}/proyects/index/${id}`;
		var out = [];
		await axios.get(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			for (obj in response.objs){
				out.push(ProyectObjcreate(obj));
			}
			this.proyectObj = out;
		});
		//logger.info(out);
		//return out;
	},
	//obtener todos
	// router.get('/', controller.list);

	async listProyect(){
		const url = `${API_URL}/cards`;
		var out = [];
		await axios.get(url , {
				headers: {'Authorization': `bearer ${token}`}
		}).then(response => {

			for (obj in response.objs){
				out.push(ProyectObjcreate(obj));
			}
			this.proyectObj = out;
		});
		//logger.info(out);
		//return out;
	},

	// //Eliminar una tarjeta
	// router.delete('/:id', controller.destroy);

	async deleteProyect(id){
		const url = `${API_URL}/proyects/index/${id}`;
		//var out = [];
		await axios.delete(url,{
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			// for (obj in response.objs){
			// 	out.push(ProyectObjcreate(obj));
			// }
			// this. = out;
			logger.info(response.message);
		});
		//logger.info(out);
		//return out;
	},

	///role
	// //Crear
	// router.post('/', controller.create);

	async createRoleObj(data){
		const url = `${API_URL}/roles/`;
		var out = [];
		await axios.post(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{

			for (obj in response.objs){
				out.push(RoleObj.create(obj));
			}
			this.roleObj = out;
		});
		//logger.info(out);
		//return out;
	},

	// //Actualizar una tarjeta por id
	// router.put('/:id', controller.update);

	async updateRoleObj(data, id){
		const url = `${API_URL}/roles/${id}`;
		var out = [];
		await axios.put(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			for (obj in response.objs){
				out.push(RoleObj.create(obj));
			}
			this.roleObj = out;
		});
		//logger.info(out);
		//return out;
	},
	// //Obtener por id
	// router.get('/index/:id', controller.index);

	async getRoleObj(data, id){
		const url = `${API_URL}/roles/index/${id}`;
		var out = [];
		await axios.get(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			for (obj in response.objs){
				out.push(RoleObj.create(obj));
			}
			this.roleObj = out;
		});
		//logger.info(out);
		//return out;
	},
	//obtener todos
	// router.get('/', controller.list);

	async listRoleObj(){
		const url = `${API_URL}/cards`;
		var out = [];
		await axios.get(url , {
				headers: {'Authorization': `bearer ${token}`}
		}).then(response => {

			for (obj in response.objs){
				out.push(RoleObj.create(obj));
			}
			this.roleObj = out;
		});
		//logger.info(out);
		//return out;
	},

	// //Eliminar una tarjeta
	// router.delete('/:id', controller.destroy);

	async deleteRoleObj(id){
		const url = `${API_URL}/roles/index/${id}`;
		//var out = [];
		await axios.delete(url,{
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			// for (obj in response.objs){
			// 	out.push(RoleObj.create(obj));
			// }
			// this. = out;
			logger.info(response.message);
		});
		//logger.info(out);
		//return out;
	},

	// //Crear
	// router.post('/', controller.create);

	async createUserObj(data){
		const url = `${API_URL}/users/`;
		var out = [];
		await axios.post(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{

			for (obj in response.objs){
				out.push(UserObj.create(obj));
			}
			this.userObj = out;
		});
		//logger.info(out);
		//return out;
	},

	// //Actualizar una tarjeta por id
	// router.put('/:id', controller.update);

	async updateUserObj(data, id){
		const url = `${API_URL}/users/${id}`;
		var out = [];
		await axios.put(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			for (obj in response.objs){
				out.push(UserObj.create(obj));
			}
			this.userObj = out;
		});
		//logger.info(out);
		//return out;
	},
	// //Obtener por id
	// router.get('/index/:id', controller.index);

	async getUserObj(data, id){
		const url = `${API_URL}/users/index/${id}`;
		var out = [];
		await axios.get(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			for (obj in response.objs){
				out.push(UserObj.create(obj));
			}
			this.userObj = out;
		});
		//logger.info(out);
		//return out;
	},

	//Obtener por id con proyectos asociados
	//router.get('/proyects/:id', controller.index);
	async getUserProy(data, id){
		const url = `${API_URL}/users/proyects/${id}`;
		var out = [];
		await axios.get(url,{
			body: data,
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			for (obj in response.objs){
				out.push(UserObj.create(obj));
			}
			this.userObj = out;
		});
		//logger.info(out);
		//return out;
	},
	//obtener todos
	// router.get('/', controller.list);

	async listUserObj(){
		const url = `${API_URL}/cards`;
		var out = [];
		await axios.get(url , {
				headers: {'Authorization': `bearer ${token}`}
		}).then(response => {

			for (obj in response.objs){
				out.push(UserObj.create(obj));
			}
			this.userObj = out;
		});
		//logger.info(out);
		//return out;
	},

	// //Eliminar una tarjeta
	// router.delete('/:id', controller.destroy);

	async deleteUserObj(id){
		const url = `${API_URL}/users/index/${id}`;
		//var out = [];
		await axios.delete(url,{
			headers: {'Authorization': `bearer ${token}`}
		})
		.then(response =>{
			// for (obj in response.objs){
			// 	out.push(UserObj.create(obj));
			// }
			// this. = out;
			logger.info(response.message);
		});
		//logger.info(out);
		//return out;
	},
	/*getTodosByPage() {
		const url = `${API_URL}/todos-page/${this.state.actualPage}`;
		axios.get(url).then(response => response.data)
			.then((data) => {
				this.state.todoList = data.data.docs;
				this.state.numberOfTodos = data.data.total;
				this.state.pages = data.data.pages;
				if(this.state.pages < this.state.actualPage){
					this.state.actualPage--;
					this.getTodosByPage();
				}else{
					for(let i = 0; i < data.data.docs.length; i++){
						this.state.todoList[i].editing  = false ;
						//Mongo regresa la fecha como un string por eso
						// para manipular como Date se tiene que castear
						this.state.todoList[i].createdAt = new Date(this.state.todoList[i].createdAt);
					}
					//this.state.todoList.splice(data.data.docs.length);
					//console.log(this.state);
				}
			});
	},
	getTodos() {
		return new Promise((resolve ,reject ) =>{
			const url = `${API_URL}/todos/`;
			axios.get(url).then(response => response.data)
				.then((data) => {
					//console.log(data);
					this.state.todoList = data;
					this.state.numberOfTodos = data.length;
					for(let i = 0; i < data.length; i++){
						this.state.todoList[i].editing  = false ;
						//Mongo regresa la fecha como un string por eso
						// para manipular como Date se tiene que castear
						this.state.todoList[i].createdAt = new Date(this.state.todoList[i].createdAt);
					}
					//console.log(this.state.todoList);
					resolve(this.state.todoList.slice(0,10));
				});
		});
	},
	getTodo(todo) {
		const url = `${API_URL}/todos/${todo}`;
		return axios.get(url).then(response => response.data);
	},*/
	login({password, email}) {
		return new Promise(
			(resolve,reject)=>{
				const url = `${API_URL}/login`;
				//console.log(todo);
				axios.post(url, { password : password, email : email })
					.then((result) => {
							//console.log(result);
							if (result.status === 200) {
								this.token = outult.data.token;
								//alert('Inicio de sesion correcto');
								//this.$router.go();
								resolve();
							}
						},(error) => {
							alert('Credenciales invalidas');
							reject();
						}
				);
			}
		);
	},/*
	updateTodo(todo) {
		const url = `${API_URL}/todos/${todo._id}`;
		axios.put(url, todo).then(
			result => {
				if (result.status === 200) {
					this.todo = outult.data;
					alert('Todo updated');
					//this.$router.go();
					this.getTodosByPage();
				}
			},
			error => {
				this.showError = true;
			}
		);
	},
	deleteTodo(todo) {
		const url = `${API_URL}/todos/${todo._id}`;
		axios.delete(url).then(response => {
			if (response.status === 200) {
				alert('Todo deleted');
				//this.$router.push({ path: '/'});
				//this.$router.go();

				this.getTodosByPage();
			}
		}).catch( err =>{
			console.error(err)
		});
	},
	enableEdit(index){
		//console.log(this.state.todoList[index]);
		if(!this.state.todoList[index].editing){
			this.state.todoList[index].editing = true;
		}else{
			this.state.todoList[index].editing = false;
		}
		this.state.todoList.splice(this.state.numberOfTodos);
		//console.log(this.state.todoList[index].editing);
	},*/

};
