/* eslint-disable no-mixed-spaces-and-tabs */
export class ProyectObj {
  name= '';
  description = '';
  date_solicited = '';
  date_start = '';
  manager = '';
  owner = '';
  equip = [''];
  history_user_cards = [''];
  closed = true;
  deleted = true;
  userObj=[{}];
  managerObjs=[{}];
  tarjetObjs=[{}];
  ownerObjs=[{}];
  
  create(obj){
  	this.name= obj.name;
  	this.description = obj.description;
  	this.date_solicited = obj.date_solicited;
  	this.date_start = obj.date_start;
  	this.manager = obj.manager;
  	this.owner = obj.owner;
  	this.equip = obj.equip;
  	this.history_user_cards = obj.history_user_cards;
  	this.closed = obj.closed;
  	this.deleted = obj.deleted;
  	this.userObjs= obj.userObjs;
  	this.managerObjs= obj.managerObjs;
  	this.tarjetObjs= obj.tarjetObjs;
  	this.ownerObjs= obj.ownerObjs;
  }
}

//module.exports = ProyectObj;
