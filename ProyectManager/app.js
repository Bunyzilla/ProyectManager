const express = require('express');
const app = express();
const router = express.Router();

const path = __dirname + '/views/';
const port = 4001;

router.use(function (req,res,next) {
  console.log('/' + req.method);
  next();
});

////////////////////////RUTAS CORRECTAS PARA SERVIR EL ARCHIVO HTML
//La ruta principal del archivo index 
router.get('/', function(req,res){
  res.sendFile(path + 'index.html');
});

router.get('/actividades', function(req,res){
    res.sendFile(path + 'actividades.html');
});

router.get('/ajustes', function(req,res){
    res.sendFile(path + 'ajustes.html');
});

router.get('/board', function(req,res){
    res.sendFile(path + 'board.html');
});

router.get('/listas', function(req,res){
    res.sendFile(path + 'listas.html');
});

router.get('/page-profile', function(req,res){
    res.sendFile(path + 'page-profile.html');
});

router.get('/proSet', function(req,res){
    res.sendFile(path + 'proSet.html');
});

router.get('/proyectos', function(req,res){
    res.sendFile(path + 'proyectos.html');
});

router.get('/registro', function(req,res){
    res.sendFile(path + 'registro.html');
});

router.get('/tarjetas', function(req,res){
    res.sendFile(path + 'tarjetas.html');
});

app.use(express.static(path));
app.use('/', router);

//Metodo para lanzar la app por el puerto
app.listen(port, function () {
  console.log('Example app listening on port '+port+'!')
});